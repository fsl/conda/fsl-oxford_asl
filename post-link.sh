if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper \
      asl_gui asl_calib asl_file asl_reg oxford_asl \
      oxford_asl_roi_stats oxford_asl_hadamard_decode \
      quasil toast
fi
